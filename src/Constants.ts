export class Constants {

  public static readonly WIDTH = 500;
  public static readonly HEIGHT = 500;
  public static readonly NMPTS = 1000;

}

export default Constants;