import * as d3 from "d3";
import { PointGenerator } from "./PointGenerator"
import { Point } from './Point'
import { SvgContainer } from "./SvgContainer";
import { Draw } from "./Draw";
import Voronoi from "./Voronoi";
import Mesh from "./Mesh";
import { Field } from "./Field";

let gen = new PointGenerator()

let t : Point[] = gen.generateRelaxedPoints(1);
let svgInstance = SvgContainer.getInstance();
let svg = svgInstance.getSvgContainer()
let mesh : Mesh = new Mesh(t, Voronoi.getInstance().getVoronoi());
let field : Field = new Field(mesh);

let hi = d3.max(field.getField) + 1e-9;
let lo = d3.min(field.getField) - 1e-9;
let mappedvals = field.getField.map(x => {
  return x > hi ? 1 : x < lo ? 0 : (x - lo) / (hi - lo);
});
let contour : number[][] = field.contour();

Draw.drawTriangulation(svg, mesh.getTris, mappedvals);
Draw.drawPathFromScratch(svg, "coast", contour);

//Draw.drawCirclesFromPoints(t, svg);