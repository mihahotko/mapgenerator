import {Point} from './Point'

export class Util {
  static pointsTo2DArray(points: Point[]) : [number, number][] {
    /*return points.map(el => {
      return [el.getX(), el.getY()]
    })*/
    let t : [number, number][] = [];
    for (let i = 0; i < points.length; i++) {
      t.push([points[i].getX(), points[i].getY()])
    }
    return t;
  }

  static arrayToPoints(array: [number, number][]) : Point[] {
    /*return array.map(el => {
      return new Point(el);
    })*/

    let t : Point[] = [];
    for (let i = 0; i < array.length; i++) {
      t.push(new Point(array[i]));
    }

    return t;

  }

}

export default Util;