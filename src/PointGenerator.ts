import * as d3 from "d3";
import {Constants} from './Constants'
import {Point} from './Point'
import Voronoi from "./Voronoi";
import Util from "./Util";

export class PointGenerator {

  private points: Point[];
  
  constructor() {
    this.points = [];
  }

  generateNormalPoints(): Point[] {
    
    this.points = d3.range(Constants.NMPTS).map(function(d) {
      return new Point([Math.random()*Constants.WIDTH, Math.random()*Constants.HEIGHT])
    })
    
    return this.points;
  }

  generateRelaxedPoints(iterations: number) : Point[] {
    let points: [number, number][] = Util.pointsTo2DArray(this.generateNormalPoints());

    let voronoi: d3.VoronoiLayout<[number, number]> = Voronoi.getInstance().getVoronoi();

    do {

      points = voronoi(points).polygons().map(d3.polygonCentroid);

      iterations--;
    } while (iterations > 0)

    return Util.arrayToPoints(points);
  }

}

export default PointGenerator;