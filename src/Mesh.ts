import Point from "./Point";
import Util from "./Util";
import Constants from "./Constants";

export class Mesh {

  private verticies : number[][];
  private edges : d3.VoronoiEdge<[number, number]>[];
  private edgesArr : (number|d3.VoronoiSite<[number, number]>)[][];
  private tris : d3.VoronoiSite<[number, number]>[][];
  private edgeIndexValues : Map<[number, number], number>;
  private adjacentIndexes : number[][];
  private edgeZero : number;
  private edgeFirst : number;

  constructor(points : Point[], voronoi : d3.VoronoiLayout<[number, number]>) {
    this.edges = voronoi(Util.pointsTo2DArray(points)).edges;
    this.edgeIndexValues = new Map<[number, number], number>();
    this.verticies = [];
    this.adjacentIndexes = [];
    this.edgesArr = [];
    this.tris = [];
    this.makeMesh();
  }

  private makeMesh() {

    for (let i = 0; i < this.edges.length; i++) {

      let edge : d3.VoronoiEdge<[number, number]> = this.edges[i];

      if (edge === undefined) {
        continue;
      }

      this.edgeZero = this.edgeIndexValues.get(edge[0]);
      this.edgeFirst = this.edgeIndexValues.get(edge[1]);

      this.setVerticies(edge);
      this.setAdjacentIndexes(edge);

      this.edgesArr.push([this.edgeZero, this.edgeFirst, edge.left, edge.right]);

      this.setTriangulation(edge);
    }

  }

  private setVerticies(edge : d3.VoronoiEdge<[number, number]>) {
      
    if (this.edgeZero === undefined) {
      this.edgeZero = this.verticies.length;
      this.edgeIndexValues.set(edge[0], this.edgeZero);
      this.verticies.push(edge[0]);

    }

    if (this.edgeFirst === undefined) {
      this.edgeFirst = this.verticies.length;
      this.edgeIndexValues.set(edge[1], this.edgeFirst);
      this.verticies.push(edge[1]);

    }

  }

  private setAdjacentIndexes(edge : d3.VoronoiEdge<[number, number]>) {
    this.adjacentIndexes[this.edgeZero] = this.adjacentIndexes[this.edgeZero] || [];
    this.adjacentIndexes[this.edgeZero].push(this.edgeZero);

    this.adjacentIndexes[this.edgeFirst] = this.adjacentIndexes[this.edgeFirst] || [];
    this.adjacentIndexes[this.edgeFirst].push(this.edgeFirst);

  }

  private setTriangulation(edge : d3.VoronoiEdge<[number, number]>) {
    this.tris[this.edgeZero] = this.tris[this.edgeZero] || [];

    if (!this.tris[this.edgeZero].includes(edge.left)) {
      this.tris[this.edgeZero].push(edge.left);
    }

    if (edge.right && !this.tris[this.edgeZero].includes(edge.right)) {
      this.tris[this.edgeZero].push(edge.right);
    }

    this.tris[this.edgeFirst] = this.tris[this.edgeFirst] || [];

    if (!this.tris[this.edgeFirst].includes(edge.left)) {
      this.tris[this.edgeFirst].push(edge.left);
    }

    if (edge.right && !this.tris[this.edgeFirst].includes(edge.right)) {
      this.tris[this.edgeFirst].push(edge.right);
    }

  }

  isEdge(index : number) : boolean {
    return this.adjacentIndexes[index].length < 3;
  }

  getNeighbours(index : number) : number[] {
    return this.adjacentIndexes[index];
  }

  isNearEdge(i : number) {
    let x = this.verticies[i][0];
    let y = this.verticies[i][1];
    let w = Constants.WIDTH;
    let h = Constants.HEIGHT;
    return x < -0.45 * w || x > 0.45 * w || y < -0.45 * h || y > 0.45 * h;
  }

  get getTris() : d3.VoronoiSite<[number, number]>[][] {
    return this.tris;
  }

  get getVerticies() : number[][] {
    return Array.from(this.verticies);
  }

  get getEdges() : (number | d3.VoronoiSite<[number, number]>)[][] {
    return this.edgesArr;
  }

}

export default Mesh;