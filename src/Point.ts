import { IPoint } from "./IPoint";

export class Point implements IPoint {

  private x: number;
  private y: number;

  constructor(point: [number, number]) {
    this.x = point[0];
    this.y = point[1];
  }

  getX(){
    return this.x;
  }
  
  getY(){
    return this.y;
  }

  setX(x: number){
    this.x = x;
  }

  setY(y: number){
    this.y = y;
  }

  getPoint() : [number, number] {
    return [this.x, this.y]
  }

}

export default Point;