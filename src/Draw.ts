import * as d3 from "d3";
import { Point } from './Point'

export class Draw {

  static drawCirclesFromPoints(points: Point[], svg: d3.Selection<SVGSVGElement, {}, HTMLElement, any>) {
    for (let i = 0; i < points.length; i++) {
      //console.log(points[i].getX());
      this.drawCircle(points[i].getX(), points[i].getY(), 3, svg);
    }
  }

  static drawCircle(x: number, y: number, r: number, svg: d3.Selection<SVGSVGElement, {}, HTMLElement, any>) {
    svg.append("circle").attr("cx", x).attr("cy", y).attr("r", r)
  }

  static makePath(path: number[][]): string {
    let pathObject: d3.Path = d3.path();
    pathObject.moveTo(path[0][0], path[0][1]);

    for (let i = 1; i < path.length; i++) {
      pathObject.lineTo(path[i][0], path[i][1]);
    }
    return pathObject.toString();
  }

  static drawTriangulation(svg: d3.Selection<SVGSVGElement, {}, HTMLElement, any>, triangulation: d3.VoronoiSite<[number, number]>[][], mappedVals: number[]) {
    let tris = svg.selectAll('path.field').data(triangulation);

    tris.enter().append('path').classed('field', true);
    tris.exit().remove();
    svg.selectAll('path.field').attr('d', Draw.makePath).style("fill", (d, i) => {
      return d3.interpolateViridis(mappedVals[i]);
    });
    //svg.selectAll('path.field').attr('d', Draw.makePath).style("fill", "none").style("stroke", "black");

  }

  static drawPathFromScratch(svg: d3.Selection<SVGSVGElement, {}, HTMLElement, any>, cls: string, paths: number[][]) {

    let pathSelection = svg.selectAll('path.' + cls).data(paths);
    pathSelection.enter()
      .append('path')
      .classed(cls, true)
    pathSelection.exit()
      .remove();
    svg.selectAll('path.' + cls)
      .attr('d', Draw.makePath).style("stroke", "black");

  }

}