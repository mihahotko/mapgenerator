import * as d3 from "d3";
import { Surface } from "./Surface";

export class Voronoi extends Surface {
  
  private static instance: Voronoi;
  private voronoiDiagram: d3.VoronoiLayout<[number, number]>;

  private constructor(){
    super();

    this.voronoiDiagram = d3.voronoi().extent([
      [-1, -1],
      [this.getWidth, this.getHeight]
    ]);

  }

  static getInstance() : Voronoi {
    if (!Voronoi.instance) {
      Voronoi.instance = new Voronoi()
    }

    return Voronoi.instance;
  }

  getVoronoi() : d3.VoronoiLayout<[number, number]>{
    return this.voronoiDiagram;
  }

}

export default Voronoi;