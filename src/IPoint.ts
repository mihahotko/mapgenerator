export interface IPoint {
  getX(): number;
  getY(): number;
  setX(x: number): void;
  setY(y: number): void;
}

export default IPoint;