import Mesh from "./Mesh";

export class Field {
  private mesh: Mesh;
  private field: number[];
  private downhill: number[];

  constructor(mesh: Mesh) {
    this.mesh = mesh;
    this.field = [];
    this.downhill = [];
    //this.zeroMesh();
    this.generateField();
  }

  private zeroMesh() {
    for (let i = 0; i < this.mesh.getVerticies.length; i++) {
      this.field[i] = 0;
    }
  }

  private generateField() {
    this.zeroMesh();
    let slope: number[] = this.addSlope(this.generateRandomVector(4));
    for (let i = 0; i < this.mesh.getVerticies.length; i++) {
      this.field[i] += slope[i];
    }
  }

  private generateDownhill() {
    for (let i = 0; i < this.field.length; i++) {
      this.downhill[i] = this.downFrom(i);
    }
  }

  private downFrom(i: number): number {

    if (this.mesh.isEdge(i)) {
      return -2;
    }

    let bestFit: number = -1;
    let bestField: number = this.field[i];
    let neigbours: number[] = this.mesh.getNeighbours(i);

    for (let i = 0; i < neigbours.length; i++) {
      if (this.field[neigbours[i]] < bestField) {
        bestField = this.field[neigbours[i]];
        bestFit = neigbours[i];
      }
    }

    return bestFit;
  }

  private addSlope(direction: number[]): number[] {
    return this.mesh.getVerticies.map(x => {
      return x[0] * direction[0] + x[1] * direction[1];
    });
  }

  private runIf(lo: number, hi: number): number {
    return lo + Math.random() * (hi - lo);
  }

  private returnNormal() {
    let z2: number = null;
    let x1 = 0;
    let x2 = 0;
    let w = 2.0;

    while (w >= 1) {
      x1 = this.runIf(-1, 1);
      x2 = this.runIf(-1, 1);
      w = x1 ** 2 + x2 ** 2;
    }

    w = Math.sqrt(-2 * Math.log(w) / w);

    return x1 * w;

  }

  contour(): number[][] {
    let level: number = 0.5;
    let edges: number[][] = this.mesh.getEdges as number[][];
    let edgesArr = [];
    for (let i = 0; i < edges.length; i++) {

      let edge: number[] = edges[i];

      if (edge[3] === undefined) {
        continue;
      }
      if (this.mesh.isNearEdge(edge[0] as number) || this.mesh.isNearEdge(edge[1] as number)) {
        continue;
      }
      if ((this.field[edge[0] as number] > level && this.field[edge[1] as number] <= level) ||
        (this.field[edge[1] as number] > level && this.field[edge[0] as number] <= level)) {
        edgesArr.push([edge[2], edge[3]]);
      }
    }

    return this.mergeSegments(edgesArr);

  }

  private mergeSegments(segments: number[][]): number[][] {
    let adjacentSegments: Map<number, number[]> = new Map<number, number[]>();

    for (let i = 0; i < segments.length; i++) {
      let seg = segments[i];
      // let a0 : number[] = adjacentSegments.get(seg[0]) || [];
      let a0: number[] = adjacentSegments.get(seg[0]) || [];
      let a1: number[] = adjacentSegments.get(seg[0]) || [];

      a0.push(seg[1]);
      a1.push(seg[0]);

      adjacentSegments.set(seg[0], a0);
      adjacentSegments.set(seg[1], a1);
    }

    let done: boolean[] = [];
    let paths: number[][] = [];
    let path: number[] = null;

    while (true) {
      if (path === null) {

        for (let i = 0; i < segments.length; i++) {

          if (done[i]) continue;

          done[i] = true;
          path = [segments[i][0], segments[i][1]];
          break;

        }
        if (path === null) break;
      }


      let changed: boolean = false;
      for (let i = 0; i < segments.length; i++) {
        if (done[i]) continue;

        if (adjacentSegments.get(path[0]).length === 2 && segments[i][0] === path[0]) {
          path.unshift(segments[i][1])
        } else if (adjacentSegments.get(path[0]).length === 2 && segments[i][1] === path[0]) {
          path.unshift(segments[i][0]);
        } else if (adjacentSegments.get(path[path.length - 1]).length === 2 && segments[i][0] === path[path.length - 1]) {
          path.push(segments[i][1]);
        } else if (adjacentSegments.get(path[path.length - 1]).length === 2 && segments[i][1] === path[path.length - 1]) {
          path.push(segments[i][0]);
        } else {
          continue;
        }

        done[i] = true;
        changed = true;
        break;
      }
      if (!changed) {
        paths.push(path);
        path = null;
      }
    }
    return paths;
  }

  private generateRandomVector(scale: number): number[] {
    return [scale * this.returnNormal(), scale * this.returnNormal()];
  }

  get getField(): number[] {
    return this.field;
  }

}

export default Field;