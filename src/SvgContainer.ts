import * as d3 from "d3";
import Surface from "./Surface";

export class SvgContainer extends Surface{
  
  private static instance: SvgContainer;
  
  private constructor() {
    super();
  }

  static getInstance() : SvgContainer {
    if(!SvgContainer.instance) {
      SvgContainer.instance = new SvgContainer();
    }

    return SvgContainer.instance;

  }

  getSvgContainer(): d3.Selection<SVGSVGElement, {}, HTMLElement, any> {
    return d3.select("body").append("svg").attr("width", this.getWidth).attr("height", this.getHeight)
  }

}