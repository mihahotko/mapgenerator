import {Constants} from './Constants'

export class Surface {

  protected static height: number = Constants.HEIGHT;
  protected static width: number = Constants.WIDTH;

  protected constructor() {

  }

  get getHeight():number {
    return Constants.HEIGHT;
  }
  
  get getWidth():number {
    return Constants.WIDTH;
  }

}

export default Surface;