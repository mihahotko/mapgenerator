class VoronoiCircleGen {

  constructor(width, height, points) {
    this.width = width
    this.height = height
    this.points = points


    this.voronoi = d3.voronoi().extent([
      [-1, -1],
      [this.width + 1, this.height + 1]
    ])

    this.relaxedPoints = this.relaxationPoints(this.points)

  }

  getMesh() {
    let edges = this.voronoi(this.relaxedPoints).edges

    let mesh = {}
    let verticiesSet = new Set()
    let edgesArr = []
    let tris = []
    let verts = []
    let edgeIndexValues = new Map()
    let adjacentIndexes = []

    for (let i = 0; i < edges.length; i++) {

      /*

      edge oblike ->
      0: array, 
      1: array,
      left: array,
      right: array

      */
      let edge = edges[i]

      if (edge === undefined) {
        continue
      }


      // get unique edges into the set
      // used if the edge exists to not get duplicate edges
      let edgeZeroIndex = edgeIndexValues.get(edge[0])
      let edgeFirstIndex = edgeIndexValues.get(edge[1])

      if (edgeZeroIndex === undefined) {
        edgeZeroIndex = verticiesSet.size
        edgeIndexValues.set(edge[0], edgeZeroIndex)
        verticiesSet.add(edge[0])
      }
      if (edgeFirstIndex === undefined) {
        edgeFirstIndex = verticiesSet.size
        edgeIndexValues.set(edge[1], edgeZeroIndex)
        verticiesSet.add(edge[1])
      }

      adjacentIndexes[edgeZeroIndex] = adjacentIndexes[edgeZeroIndex] || []

      adjacentIndexes[edgeZeroIndex].push(edgeFirstIndex)

      adjacentIndexes[edgeFirstIndex] = adjacentIndexes[edgeFirstIndex] || []

      adjacentIndexes[edgeFirstIndex].push(edgeZeroIndex)

      edgesArr.push([edgeZeroIndex, edgeFirstIndex, edge.left, edge.right])
      tris[edgeZeroIndex] = tris[edgeZeroIndex] || [];

      if (!tris[edgeZeroIndex].includes(edge.left)) {
        tris[edgeZeroIndex].push(edge.left);
      }

      if (edge.right && !tris[edgeZeroIndex].includes(edge.right)) {
        tris[edgeZeroIndex].push(edge.right);
      }

      tris[edgeFirstIndex] = tris[edgeFirstIndex] || [];

      if (!tris[edgeFirstIndex].includes(edge.left)) {
        tris[edgeFirstIndex].push(edge.left);
      }

      if (edge.right && !tris[edgeFirstIndex].includes(edge.right)) {
        tris[edgeFirstIndex].push(edge.right);
      }

    }
    verts = [...verticiesSet]
    //console.log(adjacentIndexes)
    //let triangl = this.voronoi(this.relaxedPoints).triangles().flat(1)
    //console.log(triangulation)
    mesh = {
      triangulation: tris,
      edges: edgesArr,
      verts: verts,
      adjacent: adjacentIndexes
    }

    mesh.tap = function (f) {
      let mapped = verts.map(f)
      mapped.mesh = mesh
      return mapped
    }

    return mesh

  }

  relaxationPoints(p) {

    let polygons = this.voronoi(p).polygons()
    let centroids = polygons.map(d3.polygonCentroid)

    let _this = this

    let converged = p.every(function (point, i) {
      return _this._distance(point, centroids[i]) < 1
    })

    if (!converged) {
      return this.relaxationPoints(centroids)
    } else {
      return centroids
    }
  }

  _distance(a, b) {
    return Math.sqrt(Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2))
  }

  get getRelaxedPoints() {
    return this.relaxedPoints
  }

  get getPolygons() {
    return this.voronoi(this.relaxedPoints).polygons()
  }

  get getTriangles() {
    return this.voronoi(this.points).triangles()
  }

}

class Field {
  constructor() {
  }

  addObjectToField(mesh) {
    // we pass arguments to this function and get access through the argument array property

    return this.zero(mesh)

  }

  relaxField(field) {
    let newField = this.zero(field)
    
    for(let fieldIndex = 0; fieldIndex < field.length; fieldIndex++) {
      //console.log(fieldIndex)
      var nbs = this.neighbours(field.mesh, fieldIndex)
      if (nbs.length < 3 ) {
        newField[fieldIndex] = 0
        continue
      }
      newField[fieldIndex] = d3.mean(nbs.map(j => {
        return field[j]
      }))
    }
    return newField
  }

  neighbours(mesh, index) {
   // console.log(index)
    var onbs = mesh.adjacent[index];
    var nbs = [];
    for (var i = 0; i < onbs.length; i++) {
        nbs.push(onbs[i]);
    }
    return nbs;
  }

  zero(mesh) {
    let zeroMesh = []

    for (const meshIndex in mesh.verts) {
      zeroMesh[meshIndex] = 0
    }
    zeroMesh.mesh = mesh

    return zeroMesh
  }

  generateField(mesh) {
    //TODO: zgeneriraj mesh in ga ne uporabljej v main-u
    let field = this.addObjectToField(mesh)
    
    //zakaj 10?
    /*for (let i = 0; i<1; i++) {
      field = this.relaxField(field)
    }*/
    return field
  }

}

class DrawOnSvg {

  constructor(container) {
    this.container = container
  }

  drawCircle(x, y, r) {
    this.container.append("circle").attr("cx", x).attr("cy", y).attr("r", r)
  }

  makePath(path) {
    if (path === undefined) {
      return;
    }
    let pathObject = d3.path()
    pathObject.moveTo(path[0][0], path[0][1])

    for (let i = 1; i < path.length; i++) {
      pathObject.lineTo(path[i][0], path[i][1])
    }

    return pathObject.toString()

  }

}

const width = 960
const height = 960
//const numOfPoints = 16384
const numOfPoints = 100 //testiranje
const svgContainer = d3.select("body").append("svg").attr("width", width).attr("height", height)

const points = d3.range(numOfPoints).map(function (d) {
  return [Math.random() * width, Math.random() * height]
}).sort(function (a, b) {
  return a[0] - b[0]
})




const voronoiInstance = new VoronoiCircleGen(width, height, points)

const drawOnSvgInstance = new DrawOnSvg(svgContainer)
const fieldInstance = new Field()


function visualizePoints(points) {
  points.forEach(element => {
    drawOnSvgInstance.drawCircle(element[0], element[1], 3)
  })
}

function visualizePolygons() {
  voronoiInstance.getPolygons.map(function (i) {
    svgContainer.append("path").attr("d", "M" + i.join("L") + "Z").attr("fill", "none").attr("stroke", "blue")
  })
}

let mesh = voronoiInstance.getMesh()
let generatedField = fieldInstance.generateField(mesh)
let hi = d3.max(generatedField) + 1e-9
let lo = d3.min(generatedField) - 1e-9
var mappedvals = generatedField.map(function (x) {
  return x > hi ? 1 : x < lo ? 0 : (x - lo) / (hi - lo)
});
console.log(generatedField)
//let path = drawOnSvgInstance.makePath(mesh.edges)

let tris = svgContainer.selectAll('path.field').data(generatedField.mesh.triangulation)
tris.enter().append('path').classed('field', true)
tris.exit().remove()
svgContainer.selectAll('path.field').attr('d', drawOnSvgInstance.makePath).style('fill', (d, i) => {
  return d3.interpolateCool(mappedvals[i])
}).style("stroke", "black")

console.log(mesh)
//visualizePoints(voronoiInstance.getRelaxedPoints)